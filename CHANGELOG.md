# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.3.4](https://gitlab.utkonos.ru/common/utk-scripts/compare/v0.3.3...v0.3.4) (2020-09-01)


### Features

* **sort-package-json:** add sort-package-json to lint-staged config ([7c78b9b](https://gitlab.utkonos.ru/common/utk-scripts/commit/7c78b9b62a629f61664e5ecbcb88786ddbf4d392))

### [0.3.3](https://gitlab.utkonos.ru/common/utk-scripts/compare/v0.3.2...v0.3.3) (2020-08-02)


### Features

* add check argument to release script ([a9f1fc1](https://gitlab.utkonos.ru/common/utk-scripts/commit/a9f1fc182a177a988dd1edab2fb05da942817adf))

### [0.3.2](https://gitlab.utkonos.ru/common/utk-scripts/compare/v0.3.1...v0.3.2) (2020-08-02)


### Features

* **doctoc:** add docs folder md's to precommit fix via doctoc ([82e1b2f](https://gitlab.utkonos.ru/common/utk-scripts/commit/82e1b2f4bce352926bed127883dbb02e164b4450))

### [0.3.1](https://gitlab.utkonos.ru/common/utk-scripts/compare/v0.3.0...v0.3.1) (2020-07-27)


### Bug Fixes

* add additional isWindows check ([cc2cff0](https://gitlab.utkonos.ru/common/utk-scripts/commit/cc2cff087f24066324ca3cc1cc69ebc8d2ec7e84))

## [0.3.0](https://gitlab.utkonos.ru/common/utk-scripts/compare/v0.2.4...v0.3.0) (2020-07-27)


### ⚠ BREAKING CHANGES

* Use utk-scripts instead wms-scripts in your scripts

### Features

* rename wms-scripts to utk-scripts ([8c1b228](https://gitlab.utkonos.ru/common/utk-scripts/commit/8c1b228ef4633dc04e610dfee223ce34a55376a7))


### Bug Fixes

* fix resolveUtkScripts for Windows OS ([0bb39ae](https://gitlab.utkonos.ru/common/utk-scripts/commit/0bb39aeb09f2166581d149430c13fdb1637ae491))

### [0.2.4](https://gitlab.utkonos.ru/common/utk-scripts/compare/v0.2.3...v0.2.4) (2020-07-22)

### [0.2.3](https://gitlab.utkonos.ru/common/utk-scripts/compare/v0.2.2...v0.2.3) (2020-07-22)

### [0.2.2](https://gitlab.utkonos.ru/common/utk-scripts/compare/v0.2.1...v0.2.2) (2020-07-22)


### Features

* manage dependencies: add soft versions, move release packages to peerDependencies ([5041093](https://gitlab.utkonos.ru/common/utk-scripts/commit/5041093bcb0b99e4d3b5e87af1085816944b588e))

### [0.2.1](https://gitlab.utkonos.ru/common/utk-scripts/compare/v0.2.0...v0.2.1) (2020-07-22)


### Features

* add commitlint config to package root directory ([8c6f455](https://gitlab.utkonos.ru/common/utk-scripts/commit/8c6f4550d1f3b190fd16eb198a69894af214d8e1))

## [0.2.0](https://gitlab.utkonos.ru/common/utk-scripts/compare/v0.1.4...v0.2.0) (2020-07-21)


### ⚠ BREAKING CHANGES

* After upgrade you should inslall husky itself.

### Features

* move husky to peer dependencies ([3c226fb](https://gitlab.utkonos.ru/common/utk-scripts/commit/3c226fb930744de78ae53edef44f23107ed4f77d))

### [0.1.4](https://gitlab.utkonos.ru/common/utk-scripts/compare/v0.1.3...v0.1.4) (2020-07-21)


### Features

* change node version to lts erbium ([3f0bfe4](https://gitlab.utkonos.ru/common/utk-scripts/commit/3f0bfe45fd05277ea69ffb2e9aa6dc414edff8f0))

### [0.1.3](https://gitlab.utkonos.ru/common/utk-scripts/compare/v0.1.2...v0.1.3) (2020-07-19)

### [0.1.2](https://gitlab.utkonos.ru/common/utk-scripts/compare/v0.1.1...v0.1.2) (2020-07-19)

### [0.1.1](https://gitlab.utkonos.ru/common/utk-scripts/compare/v0.1.0...v0.1.1) (2020-07-19)


### Features

* **release:** add script for generate changelog and version up ([264cbc1](https://gitlab.utkonos.ru/common/utk-scripts/commit/264cbc12d45e305941dfe21f07a5302db1413406))

## 0.1.0 (2020-07-19)

### Features

- add commitlint with conventional config ([8531c22](https://gitlab.utkonos.ru/common/utk-scripts/commit/8531c223b504c031c6c421beecfbaacf510b51ce))

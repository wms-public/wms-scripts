# utk-scripts

## Table of Contents

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Installation](#installation)
  - [Additional installation for release script](#additional-installation-for-release-script)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Installation

```sh
npm i -D git+https://gitlab.utkonos.ru/common/utk-scripts.git husky && \
echo "module.exports = require('utk-scripts/prettier');" > .prettierrc.js && \
echo "node_modules/" > .prettierignore && \
echo "module.exports = require('utk-scripts/husky');" > .huskyrc.js
echo "module.exports = require('utk-scripts/commitlint');;" > .commitlintrc.js
```

```json
{
  "scripts": {
    "validate": "utk-scripts validate lint,test"
  }
}
```

### Additional installation for release script

```sh
npm i -D conventional-recommended-bump standard-version
```

```json
{
  "scripts": {
    "release": "utk-scripts release"
  }
}
```

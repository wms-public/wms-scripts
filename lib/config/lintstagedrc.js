'use strict';

const { resolveUtkScripts, resolveBin } = require('../utils');

const utkScripts = resolveUtkScripts();
const doctoc = resolveBin('doctoc');
const sortPackageJson = resolveBin('sort-package-json');
module.exports = {
  '(README|docs/**/*).md': [`${doctoc} --maxlevel 3 --notitle`],
  '*.+(js|jsx|json|yml|yaml|css|less|scss|ts|tsx|md|graphql|mdx|vue)': [`${utkScripts} format`],
  'package.json': [sortPackageJson],
};

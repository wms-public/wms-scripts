'use strict';

const { resolveUtkScripts, hasFile } = require('../utils');

const utkScripts = resolveUtkScripts();
const huskyConfig = {
  hooks: {
    'pre-commit': `${utkScripts} pre-commit`,
  },
};

const useCommitLint = hasFile('.commitlintrc.js');
if (useCommitLint) {
  huskyConfig.hooks['commit-msg'] = 'commitlint -E HUSKY_GIT_PARAMS';
}

module.exports = huskyConfig;

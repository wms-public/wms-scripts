'use strict';

const { resolveBin, hasAnyDep } = require('../utils');
const spawn = require('cross-spawn');

const hasReleaseDeps = hasAnyDep('standard-version') && hasAnyDep('conventional-recommended-bump');

if (!hasReleaseDeps) {
  console.log('Please add standard-version and conventional-recommended-bump to your package.');
  console.log('Run `npm install -D standard-version conventional-recommended-bump`');
  process.exit(0);
}

const conventionalRecommendedBump = require('conventional-recommended-bump');

function runStandardVersion(args) {
  const result = spawn.sync(resolveBin('standard-version'), args, {
    stdio: 'inherit',
  });
  process.exit(result.status);
}

function whatBump(commits) {
  const noReleaseComments = commits.filter(
    (commit) => !(commit.type === 'chore' && commit.scope === 'release'),
  );

  if (noReleaseComments.length === 0) {
    console.log('There are no relevant changes, so no new version is released.');
    process.exit(0);
  }

  return {};
}

const args = process.argv.slice(2);

if (args.includes('--force')) {
  runStandardVersion(args.filter((a) => a !== '--force'));
}

conventionalRecommendedBump({ whatBump }, (error) => {
  if (error) throw error;

  if (args.includes('--check')) {
    console.log('New version will be released.');
    process.exit(0);
  }

  runStandardVersion(args);
});
